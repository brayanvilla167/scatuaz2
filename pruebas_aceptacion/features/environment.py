# -- FILE: features/environment.py
# CONTAINS: Browser fixture setup and teardown
from behave import fixture, use_fixture
from selenium.webdriver import Chrome,ChromeOptions
from unittest import TestCase


@fixture
def browser_firefox(context):
    # -- BEHAVE-FIXTURE: Similar to @contextlib.contextmanager
    chrome_options = ChromeOptions()
    chrome_options.add_arguments("--headless")
    context.driver = Chrome(chrome_options=chrome_options)
    context.url = 'http://localhost:8000/'
    context.test = TestCase()
    yield context.driver
    # -- CLEANUP-FIXTURE PART:
    context.driver.quit()


def before_all(context):
    use_fixture(browser_firefox, context)
    # -- NOTE: CLEANUP-FIXTURE is called after after_all() hook.
